package com.zuitt.s01a1;

import java.util.Scanner;

public class Grades {

    public static void main(String[] args) {
    int grade1, grade2, grade3, total;
    float percent;
    String fName, lName;

    Scanner input = new Scanner(System.in);

    System.out.println("Enter your first name: ");
    fName = input.nextLine();
    System.out.println("Enter your first name: ");
    lName = input.nextLine();
    System.out.println("Enter grade in English: ");
    grade1 = input.nextInt();
    System.out.println("Enter grade in Mathematics: ");
    grade2 = input.nextInt();
    System.out.println("Enter grade in Science: ");
    grade3 = input.nextInt();

    total = grade1 + grade2 + grade3;
    percent = total/3;

    System.out.println("Name: " + fName + " " + lName);
    System.out.println("Grade in English: " + grade1);
    System.out.println("Grade in Mathematics: " + grade2);
    System.out.println("Grade in Science: " + grade3);
    System.out.println("You average grade is: " + percent);
    }
}
